# Blog bulma rails

This is a blog build with [Bulma](https://bulma.io/) and [Rails](https://rubyonrails.org/). You can view a live version [here](https://blog-bulma-rails.herokuapp.com/).

## Table of contents

- [Technologies](#technologies)
- [Setup](#setup)
- [Features](#features)
- [Status](#status)
- [Test suite](#test-suite)

## Technologies

### Backend

- Ruby - 2.7.0p0
- Rails - 6.0.3.2
- Database - PostgreSQL

### Frontend

- Bulma

## Setup

- Clone the project on your machine using: `git clone https://gitlab.com/taiwo/blog_bulma_rails.git`
- Run `bundle install` to install needed gems
- Run `rails db:setup` to setup the database
- Launch the app using `rails server`
- Visit `http://localhost:3000/` in your browser

## Features

- CRUD access blogposts
- CRUD access to blogpost comments

To-do list:

- Include images to blog post
- Authors authentication
- Pagination

## Status

Project is: _in progress_

## Test suite

- You can run the test by running `bundle exec rspec`
