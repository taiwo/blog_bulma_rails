# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/comments', type: :request do
  let!(:posts) { create_list(:post, 10) }
  let!(:comments) { create_list(:comment, 10, post_id: posts.first.id) }
  let(:valid_attributes) do
    {
      name: comments.first.name,
      comment: comments.first.comment
    }
  end

  let(:invalid_attributes) do
    {
      name: nil,
      comment: nil
    }
  end

  let(:comment_id) { comments.first.id }
  let(:post_id) { comments.first.post_id }

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new comment' do
        expect do
          post post_comments_path(post_id), params: { comment: valid_attributes }
        end.to change(Comment, :count).by(1)
      end

      it 'redirects to the created comment' do
        post post_comments_path(post_id), params: { comment: valid_attributes }
        expect(response).to redirect_to(post_url(posts.first))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Comment' do
        expect do
          post post_comments_path(post_id), params: { comment: invalid_attributes }
        end.to change(Comment, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post post_comments_path(post_id), params: { comment: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested comment' do
      expect do
        delete post_comment_path(post_id, comment_id)
      end.to change(Comment, :count).by(-1)
    end

    it 'redirects to the comments list' do
      delete post_comment_path(post_id, comment_id)
      expect(response).to redirect_to(post_url(posts.first))
    end
  end
end
