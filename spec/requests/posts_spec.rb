# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Posts', type: :request do
  let!(:posts) { create_list(:post, 10) }
  let(:valid_attributes) do
    {
      title: posts.first.title,
      content: posts.first.content
    }
  end

  let(:invalid_attributes) do
    {
      title: nil,
      content: nil
    }
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      get '/posts'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /show' do
    it 'renders a successfuil response' do
      get post_url(posts.first)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_post_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'renders a successful response' do
      get edit_post_url([posts.first])
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new post' do
        expect do
          post posts_url, params: { post: valid_attributes }
        end.to change(Post, :count).by(1)
      end

      it 'redirects to the created post' do
        post posts_url, params: { post: valid_attributes }
        expect(response).to redirect_to(post_url(Post.last))
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new post' do
        expect do
          post posts_url, params: { post: invalid_attributes }
        end.to change(Post, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post posts_url, params: { post: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid paramaters' do
      let(:new_attributes) do
        {
          title: posts.second.title,
          content: posts.second.content
        }
      end

      it 'updates the requested post' do
        patch post_url(posts.first), params: { post: new_attributes }
        posts.first.reload
        expect(response).to have_http_status(302)

        follow_redirect!

        expect(response.body).to include(posts.second.title)
      end
    end

    context 'with invalid attributes' do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        patch post_url(posts.first), params: { post: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested post' do
      expect do
        delete post_url(posts.first)
      end.to change(Post, :count).by(-1)
    end
  end

  it 'redirects to the posts page' do
    delete post_url(posts.first)
    expect(response).to redirect_to(posts_url)
  end
end
