# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  before(:all) do
    @post = create(:post)
  end

  it 'should have valid attributes' do
    expect(@post).to be_valid
  end

  it 'should have a valid title' do
    @post.title = nil
    expect(@post).to_not be_valid
  end

  it 'should have a valid content' do
    @post.content = nil
    expect(@post).to_not be_valid
  end

  describe 'associations' do
    it { should have_many(:comments) }
  end
end
