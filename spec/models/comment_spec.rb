# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Comment, type: :model do
  before :all do
    @comment = create(:comment)
  end

  it 'should have valid attributes' do
    expect(@comment).to be_valid
  end

  it 'should not be valid without name' do
    @comment.name = nil
    expect(@comment).to_not be_valid
  end

  it 'should not be valid without commnet' do
    @comment.comment = nil
    expect(@comment).to_not be_valid
  end

  describe 'associations' do
    it { should belong_to(:post) }
  end
end
