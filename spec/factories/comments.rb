# frozen_string_literal: true

FactoryBot.define do
  factory :comment do
    name { Faker::Name.first_name }
    comment { Faker::Quote.yoda }
    association :post
  end
end
